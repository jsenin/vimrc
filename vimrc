"G
" TIPS
" if some keymapping fails, its easy to found it
"
" using `:map` show all key mapping
" add source .vim/vimrc to ~/.vimrc or do a symlink
" to find the owner of a key mapping:
" `:map <leader>,s` shows all keymapping matches
"

set nocompatible
filetype off

" Vundle start plugin
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Navegador de ficheros
Plugin 'scrooloose/nerdtree'

" Plugin para abrir ficheros de forma rápida con control+p
" permite tener una lista de mru -> most recent used
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'tacahiroy/ctrlp-funky'
Plugin 'ivalkeen/vim-ctrlp-tjump'

" https://github.com/bling/vim-airline
Plugin 'bling/vim-airline'

" https://github.com/tpope/vim-fugitive
Plugin 'tpope/vim-fugitive'

" easymotiion para movere por los ficheros buscando combinaciones de teclas
" se lanza con leader leader
Plugin 'easymotion/vim-easymotion'
"Plugin 'haya14busa/incsearch.vim'
"Plugin 'haya14busa/incsearch-fuzzy.vim'
"Plugin 'haya14busa/incsearch-easymotion.vim'


" https://github.com/jiangmiao/auto-pairs
" Insert or delete brackets, parens, quotes in pair.
Plugin 'jiangmiao/auto-pairs'


" https://github.com/airblade/vim-gitgutter
" muestra las lineas en las que un fichero ha cambiado en comparacion con la
" version anterior de git
Plugin 'airblade/vim-gitgutter'

" sudo apt-get install silversearcher-ag
" https://github.com/mileszs/ack.vim
Plugin 'mileszs/ack.vim'

" visual markers
Plugin 'kshenoy/vim-signature'

Plugin 'jsenin/vim-material-monokai'
" Plugin 'sheerun/vim-polyglot'
"
"* Plugin 'vim-python/python-syntax'

Plugin 'jsenin/semantic-highlight.vim'


" https://github.com/romainl/Apprentice
" Plugin 'romainl/Apprentice'

" https://github.com/tpope/vim-commentary
Plugin 'tpope/vim-commentary'

" https://github.com/suxpert/vimcaps
" sudo apt-get install libx11-dev make gcc
" desactiva capslocks cuando sales del modo insert
" Plugin 'suxpert/vimcaps'

" https://github.com/w0rp/ale
" Asynchronous linting/fixing for Vim and Language Server Protocol (LSP) integration
" Requiere desactivar el linting de python-mode

Plugin 'dense-analysis/ale'
" language server support
" https://github.com/python-lsp/python-lsp-server
" pip install --user python-lsp-server[all]

" https://github.com/ervandew/supertab
" move along popup autocompletion using TAB key
Plugin 'ervandew/supertab'

" python Autocompletion for vim
" jedi-vim uses supertab for autocompletion
" https://github.com/davidhalter/jedi-vim/blob/master/doc/jedi-vim.txt
Plugin 'davidhalter/jedi-vim'
" All of your Plugins must be added before the following line
"
" https://github.com/SirVer/ultisnips
" snippets tool, use tab for exploding words to code
Plugin 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plugin 'honza/vim-snippets'

" eyecare theme
" https://github.com/morhetz/gruvbox
Plugin 'morhetz/gruvbox'

" ropevim python refactoring
" pip install ropevim
Plugin 'python-rope/ropevim.git'

" https://github.com/fatih/vim-go
" Plugin 'fatih/vim-go'

" rainbow colorines
Plugin 'luochen1990/rainbow'

" https://github.com/wookayin/vim-autoimport
Plugin 'wookayin/vim-autoimport'

" disable rope, all linters and semantic highlighting before run this
" Plugin 'prabirshrestha/vim-lsp'
" Plugin 'prabirshrestha/asyncomplete.vim'
" Plugin 'prabirshrestha/asyncomplete-lsp.vim'

Plugin 'ludovicchabant/vim-gutentags'
Plugin 'chrisbra/unicode.vim' " Dependency required for python-imports
Plugin 'jmcantrell/vim-virtualenv' " Optional: for virtualenv support
Plugin 'mgedmin/python-imports.vim'


call vundle#end() " required


" start filetype
filetype plugin indent on   " Automatically detect file types.
syntax on                   " Syntax highlighting
set mouse=a                 " Automatically enable mouse usage
set mousehide               " Hide the mouse cursor while typing
scriptencoding utf-8

set nowrap                      " Do not wrap long lines
set autoindent                  " Indent at the same level of the previous line
set expandtab                   " Tabs are spaces, not tabs
set tabstop=2                  " An indentation every four columns
set softtabstop=2              " Let backspace delete indent
set shiftwidth=2               " Use indents of 4 spaces
set ignorecase     "ignora las mayusculas en las busquedas
set hlsearch     "ilumina los resultados de una busqueda
set incsearch

set cursorline
set number " line numbers

" tecla leader la coma
let mapleader = ','

"para poder pegar sin que idente formato presionamos f12
set pastetoggle=<F12>           " pastetoggle (sane indentation on pastes)

set t_Co=256
set background=dark
" comment because drop all colors using tmate
if exists('+termguicolors')
  set t_Co=256
  set termguicolors
  " https://superuser.com/questions/457911/in-vim-background-color-changes-on-scrolling
  " Disable Background Color Erase (BCE) so that color schemes
  " work properly when Vim is used inside tmux and GNU screen.
  set t_ut=
endif
" fix termguicolor drop colors under tmux
if &term =~# '^screen'
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

" colorscheme material-monokai
" colorscheme apprentice
colorscheme gruvbox



" evitar usar cursores en vim
" noremap <Up> <NOP>
" noremap <Down> <NOP>
" noremap <Left> <NOP>
" noremap <Right> <NOP>

" copiar al clipboarde sistema
" require la extesion +clipboard compilada en vim
" se puede instalar con apt-get install vim-gtk
vmap <C-S-C> "+y<CR>"


" ==== ControlP ====
" con control+p lista los directorios y archivos que esten en ese directorio
" lo que pernece a un control de versiones que este proximo en nivel de
" directorios
" control+f te mueves entre las opciones
" control+r activa/desactiva la busqueda por expresion regular
" control+t te lo abre un nuevo tab lo que tengas seleccionado
" control+y te crea el fichero con ese nombre y los directorios si lo has indicado
" control+p seleccionas nombre :linea te lo abre y va a esa linea
" control+x abre con split horizontal
" control+v abre con split vertical
" f5 refresca la cache de ficheros
"
" ControlP permite user expresiones reguarles
"
" Exclude files or directories using Vim's wildignore:
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/venv/*,node_modules,bower_components,*.pyc   " Linux/MacOSX

" let g:ctrlp_map = '<c-p>'
" CtrlPMixed offers a search for mru, buffer and files all together
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:15,results:15'
" maximum directory depth
let g:ctrlp_max_depth = 15
" The maximum number of input strings you want CtrlP to remember
" 5 it's enough for me
let g:ctrlp_max_history = 5

" ctrp - funky
noremap <F4> :CtrlPFunky<CR>
let g:ctrlp_funky_syntax_highlight = 1

" ------------- airline
" si airline se ve sin color hay que leer esto
" http://vim.wikia.com/wiki/256_colors_in_vim
" puede ser que sea necesario que tengas que tener esto
" en tu bashrc
" export TERM='xterm-256color'
" testealo usando este scrip
" https://raw.githubusercontent.com/incitat/eran-dotfiles/master/bin/terminalcolors.py
" tiene que salir con colores bonitos O_O

" ---- nerdtree
noremap <c-x> :NERDTreeToggle<CR>
noremap <c-f> :NERDTreeFind<CR>

" ---- ptyhon -mode
"
let g:pymode_folding = 0

" ----------------- easy motion
" reemplaza a la busqueda tradiciona con la barra
" metes texto, das al enter y te deja resaltado la letra que tienes que pulsar
" para ir a ese texto
" disable <leader><leader> default behabiour
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)

" ------ Ack / ag
" silver searcher wit ack.vim plugin
let g:ackprg = 'ag --vimgrep --ignore=composer*'
" bind leader + / to find full word under cursor
nnoremap <leader>f :Ack! '\b<cword>\b'<cr>
nnoremap <leader>F :Ack! <cword> <cr>
" ------ autopairs
let g:AutoPairsFlyMode = 0
let g:AutoPairsShortcutBackInsert = '¶'
let g:AutoPairsShortcutFastWrap= 'ł'

" ------ python-mode
let g:pymode_options_colorcolumn = 0


" ---- ultinsips" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger='<tab>'
let g:UltiSnipsJumpBackwardTrigger='<s-tab>'


" jedi-vim
" I dont want to see the definitions in the same buffer
" definitions by default <leader>d
let g:jedi#use_tabs_not_buffers = 1
" disabled because colission with Ack <leader>s
let g:jedi#goto_stubs_command = ""


" Use tabs in select mode for moving blocks
" v lowercase retain selection after move blocks
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv


" use space to toggle fold in normal and visual mode
set foldmethod=indent
set foldlevel=99
nnoremap <space> za
vnoremap <space> zf

" clear search when press esc
map <esc> :noh<esc><cr>
" replace text under cursor
" https://vim.fandom.com/wiki/Search_and_replace_the_word_under_the_cursor
nnoremap <Leader>s :%s/\<<C-r><C-w>\>/

" pres ESC ESC to clear search highlighting
nnoremap <silent> <Esc><Esc> :nohl<CR>

" replace selected text in visualmode control+r
" https://stackoverflow.com/questions/676600/vim-search-and-replace-selected-text
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" close quickfix window with ESC
augroup vimrcQfClose
    autocmd!
    autocmd FileType qf if mapcheck('<esc>', 'n') ==# '' | nnoremap <buffer><silent> <esc> :cclose<bar>lclose<CR> | endif
augroup END

" add yaml stuffs
" https://lornajane.net/posts/2018/vim-settings-for-working-with-yaml
" au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
" autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" mamba spec
au BufRead,BufNewFile *spec.py set filetype=python.spec

augroup python
  autocmd! python
  autocmd Filetype python nmap <buffer> <F5> :w<Esc>:!python %<CR>
augroup end

augroup python.spec
  autocmd! python.spec
  autocmd Filetype python.spec nmap <buffer> <F5> :w<Esc>:!mamba %<CR>
  autocmd Filetype python.spec nmap <buffer> <F6> :w<Esc>:!mamba -fdocumentation %<CR>
augroup end

" autoremove trailing whitespaces https://vim.fandom.com/wiki/Remove_unwanted_spaces
autocmd BufWritePre * :%s/\s\+$//e

" ------ ALE async linting
"
" Set this. Airline will handle the rest.
let g:airline#extensions#ale#enabled = 1

" super tab verse order when pres TAB
let g:SuperTabDefaultCompletionType = "<c-n>"
" select with enter do not behave as carriage return
let g:SuperTabCrMapping                = 1

" inline message description improveement
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
" maps ctrl+j ctrl+k to navigate arround errors
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
highlight ALEWarning ctermbg=DarkMagenta
highlight ALEError ctermbg=DarkMagenta

set completeopt=menu,menuone,preview,noselect,noinsert
" avoid preview window on python files
autocmd FileType python setlocal completeopt-=preview

let g:ale_completion_enabled = 0
" let g:ale_fixers = {'python': ['yapf']}
let g:ale_fixers = {'*': ['remove_trailing_lines'], 'python': ['ruff']}
" let g:ale_linters = {
" \   'python': ['pylsp', 'bandit'],
" \}
" let g:ale_python_pyls_executable = "pylsp"
" let g:ale_python_pyls_config = {
" \   'pylsp': {
" \     'plugins': {
" \       'pycodestyle': {
" \         'enabled': v:false,
" \       },
" \       'pyflakes': {
" \         'enabled': v:false,
" \       },
" \       'pydocstyle': {
" \         'enabled': v:false,
" \       },
" \     },
" \   },
" \}


function! SmartInsertCompletion() abort
	" Use the default CTRL-N in completion menus
	if pumvisible()
		return "\<C-n>"
	endif
	" Exit and re-enter insert mode, and use insert completion
	return "\<C-c>a\<C-n>"
endfunction

inoremap <silent> <C-n> <C-R>=SmartInsertCompletion()<CR>

""
" rainbow prentheses colors
""
let g:rainbow_active = 1 "set to 0 if you want to enable it later via :RainbowToggle



""  "In vim, how can I quickly switch between tabs?" https://superuser.com/a/675119
" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

" Go to last active tab
au TabLeave * let g:lasttab = tabpagenr()
nnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>
vnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>

let g:python_highlight_all = 1

" semantic-highlight.vim auto enable for python filetypes
let g:semanticEnableFileTypes = ['python', 'python.spec']

let ropevim_vim_completion=0
let ropevim_extended_complete=0

" autocmd FileType python setlocal omnifunc=RopeCompleteFunc
" autocmd FileType python.spec setlocal omnifunc=RopeCompleteFunc

noremap <leader>d :call RopeGotoDefinition()
noremap <leader>r :call RopeRename()
noremap <leader>i :call RopeFindOccurrences()
noremap <leader>q :q<cr>
noremap <leader>w :w<cr>
" Close quickfix list pressing ESC
nnoremap <ESC> :cclose<CR>




" para vim-lsp
"
" if executable('pylsp')
"     " pip install python-lsp-server
"     au User lsp_setup call lsp#register_server({
"         \ 'name': 'pylsp',
"         \ 'cmd': {server_info->['pylsp']},
"         \ 'allowlist': ['python'],
"         \ })
" endif

" function! s:on_lsp_buffer_enabled() abort
"     setlocal omnifunc=lsp#complete
"     setlocal signcolumn=yes
"     if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
"     nmap <buffer> gd <plug>(lsp-definition)
"     nmap <buffer> gs <plug>(lsp-document-symbol-search)
"     nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
"     nmap <buffer> gr <plug>(lsp-references)
"     nmap <buffer> gi <plug>(lsp-implementation)
"     nmap <buffer> gt <plug>(lsp-type-definition)
"     nmap <buffer> <leader>r <plug>(lsp-rename)
"     nmap <buffer> <C-k> <plug>(lsp-previous-diagnostic)
"     nmap <buffer> <C-j> <plug>(lsp-next-diagnostic)
"     nmap <buffer> K <plug>(lsp-hover)
"     nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
"     nnoremap <buffer> <expr><c-d> lsp#scroll(-4)
"     " --- custom.jorge
"     let g:lsp_diagnostics_echo_cursor = 1
"     let g:lsp_signs_enabled = 1
"     let g:lsp_diagnostics_echo_delay = 0
"     let g:lsp_diagnostics_highlights_enabled = 1
"     let g:lsp_diagnostics_signs_delay = 1
"     " enforce highligth under cursor: all references to the same word undercursor
"     " are highligthed
"     highlight lspReference ctermfg=red guifg=red ctermbg=green guibg=green

"     inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
"     inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"     inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

"     " --- custom.jorge

"     let g:lsp_format_sync_timeout = 1000
"     autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

"     " refer to doc to add more commands
" endfunction

" augroup lsp_install
"     au!
"     " call s:on_lsp_buffer_enabled only for languages that has the server registered.
"     autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
" augroup END

" move selected visual bock with mays J, K with autoident
" https://github.com/ThePrimeagen/.dotfiles/blob/master/nvim/.config/nvim/init.vim
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" copy from cursor to the end of the line with Y
nnoremap Y yg$




" Habilitar ALE
let g:ale_enabled = 1

" Configurar el linter de Ruff para ALE
let g:ale_linters = {
      \ 'javascript': ['ruff'],
      \ 'python': ['ruff'],
      \ 'ruby': ['ruff'],
      \ }

" Configurar el comando para ejecutar Ruff
let g:ale_ruff_executable = 'ruff'
" fix files on saving
let g:ale_fix_on_save = 1

" Establecer atajos de teclado para ALE
nnoremap <silent> <F7> :ALEFix<CR>
nnoremap <silent> <F8> :ALEToggle<CR>

" run ALEFix isort when execute :ImportName
let g:pythonImportsUseAleFix = 1

" Opcional: Configurar colores para los mensajes de ALE
highlight ALEErrorSign ctermfg=White ctermbg=Red
highlight ALEWarningSign ctermfg=White ctermbg=Yellow
highlight ALEInfoSign ctermfg=White ctermbg=Blue
highlight ALEError ctermfg=Red
highlight ALEWarning ctermfg=Yellow
highlight ALEInfo ctermfg=Blue





" Configuration for python-imports
let g:python_imports_autoimport = 1
let g:python_imports_autoimport_modules = 1
let g:python_imports_virtualenv_support = 1

