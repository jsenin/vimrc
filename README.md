## About
This is my vim config

### Install
``` shell
sudo apt-get install --assume-yes vim-nox vim-gtk
mv ~/.vim ~/.vim.$(date +"%F_%T")
git clone git@gitlab.com:jsenin/vimrc.git ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall
```
### Upating

Remember to run often `vim +PluginUpdate` to download plugin updates, it's not done automagically

### requirements
Debian vim, provide multiple vim packages with different supports enabled inside each one.
I'm using vim-nox for vim python support and vim-gtk for clipboard support
```
apt-get install editorconfig
```

*vim-gtk* is needed to clipboard support
```
apt-get install vim vim-nox vim-gtk
```


Requirements for avoid capslocks hell plugin.
But does not work for remote working because only works on local machine
```
sudo apt-get install libx11-dev make gcc
```

*python linters*
```
pip install 'python-language-server[all]'
pip install yapf
pip install jedi
pip install pylint


```

### extras
Silver search, search quickly inside your project files https://github.com/ggreer/the_silver_searcher
```
apt-get install silversearcher-ag
```

### Install formaters

```
npm -g install js-beautify
```
